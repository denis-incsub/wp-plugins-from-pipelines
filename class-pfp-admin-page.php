<?php

class PFP_Admin_Page {
	private $page_hook;
	private $plugins = array(
		'google-analytics-async'    => 'Beehive',
		'ultimate-branding'         => 'Branda',
		'broken-link-checker'       => 'Broken Link Checker',
		'wp-defender'               => 'Defender',
		'wp-hummingbird'            => 'Hummingbird',
		'hustle'                    => 'Hustle',
		'wp-plugins-from-pipelines' => 'Plugins from Pipelines',
		'wpmu-dev-seo'              => 'SmartCrawl',
		'wp-smushit'                => 'Smush',
		'wpmudev-videos'            => 'Integrated Video Tutorials',
	);

	function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'register_assets' ), - 10 );
		add_action( 'admin_menu', array( $this, 'add_page' ), 20 );
	}

	public function register_assets( $hook ) {
		if ( $hook !== $this->page_hook ) {
			return;
		}

		wp_register_script( 'pfp-admin-page', plugin_dir_url( __FILE__ ) . 'js/admin-page.js', array( 'jquery' ), '1.0.0', true );
		wp_localize_script( 'pfp-admin-page', 'pfp_vars', array(
			'onboarding_done' => get_option( Plugins_From_Pipelines::ONBOARDING_DONE_OPTION_ID ),
		) );
		wp_enqueue_style( 'pfp-admin-page-styling', plugin_dir_url( __FILE__ ) . 'css/admin-page.css', array(), '1.0.0', 'all' );
	}

	public function add_page() {
		$this->page_hook = add_menu_page(
			esc_html__( 'Plugins From Pipelines', 'pfp' ),
			esc_html__( 'Plugins From Pipelines', 'pfp' ),
			'manage_options',
			'pfp_admin_page',
			array( $this, 'admin_page' ),
			''
		);
	}

	public function admin_page() {
		wp_enqueue_script( 'pfp-admin-page' );

		$onboarding_done = get_option( Plugins_From_Pipelines::ONBOARDING_DONE_OPTION_ID );

		?>
		<div class="wrap" id="pfp-page-wrap">
			<h1><?php esc_html_e( 'Plugins From Pipelines', 'pfp' ); ?></h1>

			<div id="pfp-container"
			     class="pfp-box"
			     style="<?php echo $onboarding_done ? '' : 'display:none;'; ?>">

				<?php wp_nonce_field( 'pfp_ajax_call' ) ?>

				<div class="pfp-box-body">
					<div id="pfp-select-plugin" class="pfp-loading">
						<label for="plugin"><?php esc_attr_e( 'Select a plugin', 'pfp' ); ?></label>
						<select id="plugin">
							<?php foreach ( $this->plugins as $plugin_repo_slug => $plugin_name ): ?>
								<option value="<?php echo esc_attr( $plugin_repo_slug ); ?>">
									<?php echo esc_html( $plugin_name ); ?>
								</option>
							<?php endforeach; ?>
						</select>

						<button id="pfp-select-plugin-button" class="button button-primary">
							<?php esc_html_e( 'Go', 'pfp' ); ?>
						</button>
					</div>

					<div id="pfp-select-build" style="display: none;">
						<label for="build"><?php esc_attr_e( 'Select a version', 'pfp' ); ?></label>
						<div id="pfp-select-build-inner"></div>
						<button id="pfp-select-build-button"
						        class="button button-primary">

							<?php esc_html_e( 'Install', 'pfp' ); ?>
						</button>
						<a id="pfp-abort-select-build-button"
						   href="<?php echo admin_url( 'admin.php?page=pfp_admin_page' ); ?>"
						   class="button">

							<?php esc_html_e( 'Back', 'pfp' ); ?>
						</a>
					</div>

					<div id="pfp-installed" style="display:none;">
						<?php esc_html_e( 'Installed!', 'pfp' ); ?> &#127881;
						<a class="button button-primary"
						   href="<?php echo admin_url( 'admin.php?page=pfp_admin_page' ); ?>">
							<?php esc_html_e( 'Install Another', 'pfp' ); ?>
						</a>
					</div>

					<div id="pfp-error" style="display: none;">
						<?php esc_html_e( 'Error!', 'pfp' ); ?> <span>&#9888;</span>
						<a class="button button-primary"
						   href="<?php echo admin_url( 'admin.php?page=pfp_admin_page' ); ?>">
							<?php esc_html_e( 'Try Again', 'pfp' ); ?>
						</a>
					</div>
				</div>
			</div>

			<div id="pfp-onboarding" class="pfp-box <?php echo $onboarding_done ? 'pfp-box-close' : ''; ?>">
				<div class="pfp-box-head">
					<h2><span>&#129171;</span> <?php esc_html_e( 'Get Started', 'pfp' ); ?></h2>
				</div>
				<div class="pfp-box-body">
					<img src="<?php echo plugin_dir_url( __FILE__ ) . '/images/bitbucket-credentials.gif'; ?>" alt=""/>
					<label><?php esc_html_e( 'Step 1: Get BitBucket API Credentials', 'pfp' ); ?></label>
					<ul>
						<li><?php esc_html_e( 'Go to BitBucket and click on your profile image in the bottom left corner', 'pfp' ); ?></li>
						<li><?php esc_html_e( 'Select your name', 'pfp' ); ?></li>
						<li><?php esc_html_e( 'Click "Settings" and then "OAuth consumers"', 'pfp' ); ?></li>
						<li><?php esc_html_e( 'Add a new consumer', 'pfp' ); ?></li>
						<li><?php esc_html_e( 'Make sure a valid URL is entered in the "Callback URL" field', 'pfp' ); ?></li>
						<li><?php esc_html_e( 'Make sure "This is a private consumer" is checked', 'pfp' ); ?></li>
						<li><?php esc_html_e( 'Under permissions grant read access to "Repositories" and "Pipelines"', 'pfp' ); ?></li>
						<li><?php esc_html_e( 'Click the save button', 'pfp' ); ?></li>
						<li><?php esc_html_e( 'Click the name of the new consumer and you will be shown a key and a secret', 'pfp' ); ?></li>
					</ul>

					<label><?php esc_html_e( 'Step 2: Define new constants', 'pfp' ); ?></label>
					<ul>
						<li><?php esc_html_e( 'In your WP config file define the following constants:', 'pfp' ); ?></li>
					</ul>
					<pre>define( 'PFP_BITBUCKET_CLIENT_ID', 'YOUR_KEY_HERE' );</pre>
					<pre>define( 'PFP_BITBUCKET_CLIENT_SECRET', 'YOUR_SECRET_HERE' );</pre>

					<?php if ( ! $onboarding_done ): ?>
						<p></p>
						<button id="pfp-onboarding-done" class="button button-primary">
							<?php esc_html_e( 'Got It!', 'pfp' ); ?>
						</button>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php
	}
}
